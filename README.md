# README #

A Repository that covers a wide breadth of Basic Programming Problems in C++.
Specially useful for Mac users who wants to jump start in Competitive programming world with out extra setup required. (With respective Time Complexity)

### Topics Covered ###
1. Common C++ Problems         (**0** Programs)   
2. String Manipulation       (**0** Programs)
3. Pattern Printing          (**0** Programs)
4. Bits Manipulation         (**0** Programs)
5. Stack                     (**0** Programs)
6. Linked List               (**0** Programs)
7. Queue                     (**0** Programs)
8. Divide And Conquer Approach(**0** Programs)
9. Dynamic Programming Approach(**0** Programs)
10. Greedy Approach          (**0** Programs)
11. Recursion Approach          (**8** Programs)


   



### Platform Version Info ###

* A Command line app
* XCode 8.1 
* OSX 10.11 El Captain


### In Case of any suggestion or issue :###

* Mail : therahulgoel@gmail.com