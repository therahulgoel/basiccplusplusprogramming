//
//  Backtracking.cpp
//  CPlusPlusProgramming
//
//  Created by RAHUL GOEL on 06/11/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "NQueenProb.hpp"

//As we can not have an static array whose size is variable in C++ so took a macro for the same
#define size  4
int board[size][size];

void printBoardState();

void printBoardState(){
    printf("\nBoard State is :\n");
    for (int i =0;i<size; i++) {
        for (int j=0; j<size; j++) {
            printf("%d ",board[i][j]);
        }
        printf("\n");
    }
}

void NQueen(int n){
    printBoardState();
}

