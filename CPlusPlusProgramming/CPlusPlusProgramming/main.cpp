//
//  main.cpp
//  CPlusPlusProgramming
//
//  Created by RAHUL GOEL on 06/11/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "FilesIncluded.h"

void alicAndBob(int a);

using namespace std;

int main(int argc, const char * argv[]) {
    int n;
    cin >> n;
    while (n) {
        int val;
        cin >> val;
        alicAndBob(val);
        n--;
    }
    
    return 0;
}

void alicAndBob(int a){
    int count =0,i= 2,div=2,j=2 ;
    while (i<=a) {
        while (j<=sqrt(i)) {
            if (i%j == 0) {
                break;
            }else{
                count++;
            }
        }
        i++;
    }
    
    if(a == 0  || a== 1){
        cout << "Bob";
        return;
    }
    
    if (count%2 == 0){
        cout << "Bob";
    }else{
        cout << "Alice";
    }
    cout << "\n";
}


